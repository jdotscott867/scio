# Bitbucket - setting up and using ssh #

## Enabling ssh on a mac ##

Reference - https://confluence.atlassian.com/bitbucket/set-up-an-ssh-key-728138079.html

``` bash
# Generate private and public keys
$ ssh-keygen
    Generating public/private rsa key pair.
    Enter file in which to save the key (/Users/jsmith/.ssh/id_rsa): 
    Enter passphrase (empty for no passphrase): 
    Enter same passphrase again: 
    Your identification has been saved in /Users/jsmith/.ssh/id_rsa.
    Your public key has been saved in /Users/jsmith/.ssh/id_rsa.pub.
    The key fingerprint is:
    SHA256:IuaXuywMmghCooNVhNXxrNMPnvgnOiUeL1iJ9oOGHfc jsmith@MAC-54301
    The key's randomart image is:
    +---[RSA 2048]----+
    |   +o...         |
    |  . . .o         |
    |   .    o        |
    |...    o         |
    |=.  o.+.S        |
    |* .o+.**.+       |
    |++ *.O+*o .      |
    |+ . Bo*oE .      |
    |   . .=*.o       |
    +----[SHA256]-----+

# Add key to local .ssh setup
$ eval `ssh-agent`
    Agent pid 31028

$ ssh-add -K id_rsa
    Enter passphrase for id_rsa: 
    Identity added: id_rsa (jsmith@MAC-54301)

# Test ssh access 
$ ssh -T git@bitbucket.org
    logged in as jdotscott867
    You can use git or hg to connect to Bitbucket. Shell access is disabled

# Git clone example
$ git clone git@bitbucket.org:<userID>/example.git
```