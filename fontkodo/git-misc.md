# Git - Miscellaneous Items #

## Find these a home ##

### git push - Remote end hung up ###

This error occurs when the objects being written exceeds the http post buffer size.

Error:

``` text
$ git push
    Enumerating objects: 18190, done.
    Counting objects: 100% (18190/18190), done.
    Delta compression using up to 4 threads
    Compressing objects: 100% (15669/15669), done.
    error: RPC failed; HTTP 403 curl 55 SSL_write() returned SYSCALL, errno = 32
    fatal: the remote end hung up unexpectedly
    Writing objects: 100% (18189/18189), 121.38 MiB | 10.07 MiB/s, done.
    Total 18189 (delta 5055), reused 0 (delta 0)
    fatal: the remote end hung up unexpectedly
    Everything up-to-date
```

Resolution:

`git config --global http.postBuffer <size in bytes>`

Example - setting to 150MB:

`git config --global http.postBuffer 157286400`