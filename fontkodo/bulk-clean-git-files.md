# Remove git control files and such #

The following commands are in bash ...

``` bash
for i in {1..100}; do curl -u "<username>:<github_token" -s https://github.com/api/v3/orgs/<my_org>/repos?page=${i} | jq .[].html_url | xargs -n 1 git clone ; done

find . -name ".gitignore" -exec rm -rf "{}" \;
find . -name ".idea" -exec rm -r "{}" \;
```

## Empty directories ##

``` bash
find . -name ".idea" -type d -delete
```
